#include "PLC.hpp"
#include <Python.h>

#include <iostream>
#include <thread>
#include <mutex>


static PyObject *pLogixName;
static PyObject *pLogixModule;


//Integration of functions needed to talk with PLC
//The header and IP adress are in the python file plcComm.py

static bool threadRunning;
static bool isPendingReadOrder;
static bool isPendingWriteOrder;
static int  tagReadValue;
static int  tagWriteValue;
static std::string readString;
static std::string writeString;
static bool isThreadStarted = true;

void PLC_Init(void);
void PLC_WriteTag(const std::string &tagName, int valueToWrite);
int  PLC_ReadTag(const std::string &tagName);
void PLC_End(void);
void PLCCommThread(void);

static std::thread PLCThread(PLCCommThread);
static std::mutex PLCCommMutex;

void PLC_Init(void)
{
	Py_Initialize();
	pLogixName = PyUnicode_FromString("plcComm");
	pLogixModule = PyImport_Import(pLogixName); //For this to work, i added plcComm into pythonpath in file ~/.bashrc
	PyObject *pFunc = PyObject_GetAttrString(pLogixModule, "init");
	PyObject *pValue = PyObject_CallObject(pFunc, NULL);//NULL instead of py_args if no arguments*/
}

void PLC_End(void)
{
	PyObject *pFunc = PyObject_GetAttrString(pLogixModule, "end");
	PyObject *pValue = PyObject_CallObject(pFunc, NULL);//NULL instead of py_args if no arguments*/
	Py_Finalize();
}

void PLC_WriteTag(const std::string &tagName, int valueToWrite)
{
	PyObject *pFunc = PyObject_GetAttrString(pLogixModule, "writeTag");
	PyObject *py_args = PyTuple_New(2);
	PyTuple_SetItem(py_args, 0, PyUnicode_FromString(tagName.c_str()));
	PyTuple_SetItem(py_args, 1, PyLong_FromLong(valueToWrite));
	PyObject *pValue = PyObject_CallObject(pFunc, py_args);//NULL instead of py_args if no arguments*/
}

int PLC_ReadTag(const std::string &tagName)
{
	PyObject *pFunc = PyObject_GetAttrString(pLogixModule, "readTag");
	PyObject *py_args = PyTuple_New(1);
	PyTuple_SetItem(py_args, 0, PyUnicode_FromString(tagName.c_str()));
	PyObject *pValue = PyObject_CallObject(pFunc, py_args);//NULL instead of py_args if no arguments*/
	PyObject *readValue = PyObject_GetAttrString(pValue, "Value");
	int retval = PyLong_AsLong(readValue);

	return retval;
}

void PLCCommThread(void)
{
  PLC_Init();
  {
    const std::lock_guard<std::mutex> lock(PLCCommMutex);
    threadRunning = true;
  }
  while(threadRunning)
  {
    const std::lock_guard<std::mutex> lock(PLCCommMutex);
    if(isPendingReadOrder)
    {
      isPendingReadOrder = false;
      tagReadValue = PLC_ReadTag(readString);
    }
    if(isPendingWriteOrder)
    {
      isPendingWriteOrder = false;
      PLC_WriteTag(writeString, tagWriteValue);
    }
  }
  PLC_End();
}


void PLC_Request_WriteTag(const std::string &tagName, int valueToWrite)
{
	bool success = false;
	while(!success)
	{
		const std::lock_guard<std::mutex> lock(PLCCommMutex);
		if(!isPendingWriteOrder)
		{
			isPendingWriteOrder = true;
			tagWriteValue = valueToWrite;
			writeString = tagName;
			success = true;
		}
	}
}

int  PLC_Request_ReadTag(const std::string &tagName)
{
	bool success = false;
	while(!success)
	{
		const std::lock_guard<std::mutex> lock(PLCCommMutex);
		if(!isPendingReadOrder)
		{
			isPendingReadOrder = true;
			readString = tagName;
			success = true;
		}
	}
  success = false;
	int retval;
	while(!success)
	{
		const std::lock_guard<std::mutex> lock(PLCCommMutex);
		if(!isPendingReadOrder)
		{
			retval = tagReadValue;
			success = true;
		}
	}
	return retval;
}

void PLC_Request_Init(void)
{
	if(!isThreadStarted)
	{
		isThreadStarted = true;
		PLCThread = std::thread(PLCCommThread);
	}
}

void PLC_Request_End(void)//TODO : mapper ca a la fermeture de l'IHM
{
	if(isThreadStarted)
	{
		{
			const std::lock_guard<std::mutex> lock(PLCCommMutex);
			threadRunning = false;
		}
		PLCThread.join();
		isThreadStarted = false;
	}
}
