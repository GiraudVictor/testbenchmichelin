#include "UR10.hpp"

#include <assert.h>

#include "ros/ros.h"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include "std_msgs/Float64MultiArray.h"

#include <controller_manager_msgs/SwitchController.h>
#include "../common/PoseRPY.h"

#include "../common/positionConverter.hpp"
#include "robotTopicNames.hpp"


static currentRobotController arm1CurrentController = NO_CONTROLLER;
static currentRobotController arm2CurrentController = NO_CONTROLLER;

static void positionCallbackArm1(const geometry_msgs::Pose::ConstPtr& msg);
static void positionCallbackArm2(const geometry_msgs::Pose::ConstPtr& msg);

static geometry_msgs::Pose poseArm1;
static geometry_msgs::Pose poseArm2;

//TODO : creer une constexpr pour l'entete des commandes. Ca doit se faire a compile time.

class rosHandler //TODO : transformer ca en singleton. A la construction, stop le scaled_pos_traj
{
public:
	//ros::NodeHandle nh;

	ros::ServiceClient serviceArm1Switch;
	ros::ServiceClient serviceArm2Switch;

	ros::Publisher pubUR10JointPositionArm1;
	ros::Publisher pubUR10JointPositionArm2;

	ros::Publisher pubUR10PositionArm1;
	ros::Publisher pubUR10PositionArm2;

	ros::Publisher pubUR10SpeedArm1;
	ros::Publisher pubUR10SpeedArm2;

	ros::Subscriber subUR10RobotPositionCartesianArm1;//Position subscriber for cartesian controller. Encapsulate and publish for sensors as relative position.
	ros::Subscriber subUR10RobotPositionCartesianArm2;

	ros::Subscriber subUR10RobotPositionVelocityArm1;//Position subscriber for vel_based_cartesian_velocity_controller. Encapsulate and publish for sensors as relative position.
	ros::Subscriber subUR10RobotPositionVelocityArm2;
};

static rosHandler rosScoper;



void UR10RosInit(void)
{
	std::cout << "debug bras" << std::endl;

	ros::NodeHandle nh;
	//rosScoper.nh = nh;

	std::string paramString1, paramString2;
	paramString1 = topicNameArm1;
	paramString2 = topicNameArm2;
	paramString1.append("/controller_manager/switch_controller");
	paramString2.append("/controller_manager/switch_controller");
	rosScoper.serviceArm1Switch = nh.serviceClient<controller_manager_msgs::SwitchController>(paramString1);
	rosScoper.serviceArm2Switch = nh.serviceClient<controller_manager_msgs::SwitchController>(paramString2);

	paramString1 = topicNameArm1;
	paramString2 = topicNameArm2;
	paramString1.append("/joint_array_control/command");
	paramString2.append("/joint_array_control/command");
	rosScoper.pubUR10JointPositionArm1 = nh.advertise<std_msgs::Float64MultiArray>(paramString1, 1);
	rosScoper.pubUR10JointPositionArm2 = nh.advertise<std_msgs::Float64MultiArray>(paramString2, 1);

	paramString1 = topicNameArm1;
	paramString2 = topicNameArm2;
	paramString1.append("/cartesian_velocity_control/command");
	paramString2.append("/cartesian_velocity_control/command");
	rosScoper.pubUR10PositionArm1 = nh.advertise<campero_ur_ip_controllers::PoseRPY>(paramString1, 1);
	rosScoper.pubUR10PositionArm2 = nh.advertise<campero_ur_ip_controllers::PoseRPY>(paramString2, 1);

	paramString1 = topicNameArm1;
	paramString2 = topicNameArm2;
	paramString1.append("/vel_based_cartesian_velocity_control/command");
	paramString2.append("/vel_based_cartesian_velocity_control/command");
	rosScoper.pubUR10SpeedArm1 = nh.advertise<geometry_msgs::Twist>(paramString1, 1);
	rosScoper.pubUR10SpeedArm2 = nh.advertise<geometry_msgs::Twist>(paramString2, 1);

	paramString1 = topicNameArm1;
	paramString2 = topicNameArm2;
	paramString1.append("/cartesian_velocity_control/current_x");
	paramString2.append("/cartesian_velocity_control/current_x");
	rosScoper.subUR10RobotPositionCartesianArm1 = nh.subscribe(	paramString1, 1, positionCallbackArm1);
	rosScoper.subUR10RobotPositionCartesianArm2 = nh.subscribe(	paramString2, 1, positionCallbackArm2);

	paramString1 = topicNameArm1;
	paramString2 = topicNameArm2;
	paramString1.append("/vel_based_cartesian_velocity_control/current_x");
	paramString2.append("/vel_based_cartesian_velocity_control/current_x");
	rosScoper.subUR10RobotPositionVelocityArm1 = nh.subscribe(paramString1, 1, positionCallbackArm1);
	rosScoper.subUR10RobotPositionVelocityArm2 = nh.subscribe(paramString2, 1, positionCallbackArm2);
}

void stopCurrentController(currentRobotArm armToStop)
{
	currentRobotController currentArmController;
	if(ARM1 == armToStop)
	{
		currentArmController = arm1CurrentController;
		arm1CurrentController = NO_CONTROLLER;
	}
	else
	{
		assert(ARM2 == armToStop);
		currentArmController = arm2CurrentController;
		arm2CurrentController = NO_CONTROLLER;
	}

	std::string controllerName;
	switch(currentArmController)
	{
		case NO_CONTROLLER:
			return;
			break;
		case SPEED_CONTROLLER:
			ROS_INFO("Trying to stop speed control");

			controllerName = "vel_based_cartesian_velocity_control";
			break;
		case POSITION_CONTROLLER:
			controllerName = "cartesian_velocity_control";
			break;
		case JOINT_CONTROLLER:
			controllerName = "joint_array_control";
			break;
	}

	controller_manager_msgs::SwitchController srv;
 	srv.request.strictness = srv.request.STRICT;
	srv.request.stop_controllers.push_back(controllerName);

	if(ARM1 == armToStop)
	{
		rosScoper.serviceArm1Switch.call(srv);
	}
	else
	{
		rosScoper.serviceArm2Switch.call(srv);
	}


	//ASSERT_TRUE(call_success);
    //EXPECT_TRUE(srv.response.ok);

	return;
}

void startCurrentController(currentRobotArm armToStart, currentRobotController controllerToStart)
{
	controller_manager_msgs::SwitchController srv;
 	srv.request.strictness = srv.request.STRICT;
	std::string controllerName;
	switch(controllerToStart)
	{
		case NO_CONTROLLER:
			return;
			break;
		case SPEED_CONTROLLER:
			controllerName = "vel_based_cartesian_velocity_control";
			break;
		case POSITION_CONTROLLER:
			controllerName = "cartesian_velocity_control";
			break;
		case JOINT_CONTROLLER:
			controllerName = "joint_array_control";
			break;
	}
	srv.request.start_controllers.push_back(controllerName);

	if(ARM1 == armToStart)
	{
		if(controllerToStart != arm1CurrentController)
		{
			arm1CurrentController = controllerToStart;
			rosScoper.serviceArm1Switch.call(srv);
		}
		else
		{
			return;
		}
	}
	else
	{
		assert(ARM2 == armToStart);
		if(controllerToStart != arm2CurrentController)
		{
			arm2CurrentController = controllerToStart;
			rosScoper.serviceArm2Switch.call(srv);
		}
		else
		{
			return;
		}
	}

	//ASSERT_TRUE(call_success);
    //EXPECT_TRUE(srv.response.ok);
}

void jointMoveArm(currentRobotArm armToMove, const std::vector<float> &jointOrder_rad)
{
	std_msgs::Float64MultiArray array;
	array.data.clear();

	if(jointOrder_rad.size() != 6)
	{
		throw("Program error : joint command not of the right size");
	}

	for(float f:jointOrder_rad)
	{
		array.data.push_back(f);
	}

	if(ARM1 == armToMove)
	{
		rosScoper.pubUR10JointPositionArm1.publish(array);
	}
	else
	{
		rosScoper.pubUR10JointPositionArm2.publish(array);
	}
}



void cartesianPositionMoveArm(currentRobotArm armToMove, pose targetPose)
{
	campero_ur_ip_controllers::PoseRPY msgToSend;
	msgToSend.id = 0;

	if(ARM1 == armToMove)
	{
		pose convertedPose = UnifiedPositionToEndEffector1(targetPose);

		msgToSend.position.x = convertedPose.x;
		msgToSend.position.y = convertedPose.y;
		msgToSend.position.z = convertedPose.z;
		msgToSend.orientation.roll = convertedPose.roll;
		msgToSend.orientation.pitch = convertedPose.pitch;
		msgToSend.orientation.yaw = convertedPose.yaw;


		std::cout << "moving arm from" << poseArm1.position.x << "  " << poseArm1.position.y << "  " << poseArm1.position.z << std::endl;
		std::cout << "to (relative)" << convertedPose.x << "  " << convertedPose.y << "  " << convertedPose.z << std::endl;
		std::cout << "to (absolute)" << targetPose.x << "  " << targetPose.y << "  " << targetPose.z << std::endl;


		rosScoper.pubUR10PositionArm1.publish(msgToSend);
	}
	else
	{
		pose convertedPose = UnifiedPositionToEndEffector2(targetPose);

		msgToSend.position.x = convertedPose.x;
		msgToSend.position.y = convertedPose.y;
		msgToSend.position.z = convertedPose.z;
		msgToSend.orientation.roll = convertedPose.roll;
		msgToSend.orientation.pitch = convertedPose.pitch;
		msgToSend.orientation.yaw = convertedPose.yaw;

		std::cout << "moving arm from" << poseArm2.position.x << "  " << poseArm2.position.y << "  " << poseArm2.position.z << std::endl;
		std::cout << "to (relative)" << convertedPose.x << "  " << convertedPose.y << "  " << convertedPose.z << std::endl;
		std::cout << "to (absolute)" << targetPose.x << "  " << targetPose.y << "  " << targetPose.z << std::endl;

		rosScoper.pubUR10PositionArm2.publish(msgToSend);
	}

}


void cartesianVelocityMoveArm(currentRobotArm armToMove, pose armSpeedOrder)
{
	geometry_msgs::Twist msg;

	msg.linear.x = armSpeedOrder.x;
	msg.linear.y = armSpeedOrder.y;
	msg.linear.z = armSpeedOrder.z;

	msg.angular.x = armSpeedOrder.roll;
	msg.angular.y = armSpeedOrder.pitch;
	msg.angular.z = armSpeedOrder.yaw;

	if(ARM1 == armToMove)
	{
		rosScoper.pubUR10SpeedArm1.publish(msg);
	}
	else
	{
		rosScoper.pubUR10SpeedArm2.publish(msg);
	}
}

static void positionCallbackArm1(const geometry_msgs::Pose::ConstPtr& msg)
{
	poseArm1.position = msg->position;
	poseArm1.orientation = msg->orientation;
}

static void positionCallbackArm2(const geometry_msgs::Pose::ConstPtr& msg)
{
	poseArm2.position = msg->position;
	poseArm2.orientation = msg->orientation;
}

pose getArm1Pose(void)
{
	pose Ori = quaternionToRPY(poseArm1.orientation);

	//std::cout << "Absolute frame" << poseArm1.position.x << "  " << poseArm1.position.y << "  " << poseArm1.position.z << std::endl;

	pose retval(poseArm1.position.x, poseArm1.position.y, poseArm1.position.z, Ori.roll, Ori.pitch, Ori.yaw);
	retval = EndEffector1ToUnifiedPosition(retval);
	//std::cout << "Relative frame" << retval.x << "  " << retval.y << "  " << retval.z << std::endl;
	return(retval);
}

pose getArm2Pose(void)
{
	pose Ori = quaternionToRPY(poseArm2.orientation);
	pose retval(poseArm2.position.x, poseArm2.position.y, poseArm2.position.z, Ori.roll, Ori.pitch, Ori.yaw);
	retval = EndEffector2ToUnifiedPosition(retval);
	return(retval);
}
