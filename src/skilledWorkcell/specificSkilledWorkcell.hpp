#pragma once

#include "../common/genericLogic.hpp"
#include "../common/genericSkilledWorkcell.hpp"

#include <vector>
#include <string>

//TODO : colle pas a mon template, corriger le template

skilledWorkcellInterface& 	getSkilledWorkcellInterface		(void);
nodeID 						specificSkilledWorkcellGetMaster(void);
void						specificSkilledWorkcellRosInit	(void);

void			specificSkilledWorkcellControl(std::string commandString, std::string sensorString);
void						specificSkilledWorkcell_setSensor(std::string);

//Custom declaration of inherited interface
class michelinSkilledWorkcellInterface: public skilledWorkcellInterface
{
public:
	void skilledWorkcellInit(void) override;
	void skilledWorkcellIdle(void) override;
	void skilledWorkcellStop(void) override;

	void skilledWorkcellPerformT1S2(void) override; //IHM Control
	void skilledWorkcellGraspingT1S3(void) override; //Precise joining of 2 layers
	void skilledWorkcellPerformT1S3(void) override;
};
