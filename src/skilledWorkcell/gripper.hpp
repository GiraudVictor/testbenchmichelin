#ifndef GRIPPER_HPP
#define GRIPPER_HPP

void gripper_init(int gripperNumber);
void gripper_open(int gripperNumber);
void gripper_close(int gripperNumber);

#endif //GRIPPER_HPP
