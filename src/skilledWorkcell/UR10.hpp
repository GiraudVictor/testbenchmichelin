#ifndef UR10_HPP
#define UR10_HPP

#include "../common/genericLogic.hpp"

typedef enum
{
	ARM1,
	ARM2
}currentRobotArm;

typedef enum
{
	NO_CONTROLLER,
	SPEED_CONTROLLER,
	POSITION_CONTROLLER,
	JOINT_CONTROLLER
}currentRobotController;

void UR10RosInit(void);

//I should add functions and stuff to get current position. How so? Joint array then cinematics?

void stopCurrentController(currentRobotArm armToStop);
void startCurrentController(currentRobotArm armToStart, currentRobotController controllerToStart);

void jointMoveArm(currentRobotArm armToMove, const std::vector<float> &jointOrder_rad);
void cartesianPositionMoveArm(currentRobotArm armToMove, pose armTargetPose);
void cartesianVelocityMoveArm(currentRobotArm armToMove, pose armSpeedOrder); 

pose getArm1Pose(void); //Return relative pose of the arm
pose getArm2Pose(void); //Return relative pose of the arm

#endif //UR10_HPP
