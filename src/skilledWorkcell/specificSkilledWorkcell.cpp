#include "specificSkilledWorkcell.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <string>

#include <ros/ros.h>
#include <std_msgs/Bool.h>

#include "../common/PoseRPY.h"

#include "UR10.hpp"
#include "gripper.hpp"
#include "PLC.hpp"

static nodeID currentMaster = SKILLED_WORKCELL;//Switch this in different state so that
static softmanbotState lastOrder = IDLE;//I want to do interfaces actions once, rest is on callbacks. Thus, I have to keep memory of last action treated

static std::vector<float> arm1RestJointOrder{-0.13, -0.54, -2.76, -1.87, 0.07, 1.51};
static std::vector<float> arm1IntermediaryJointOrder{-0.32, -1.10, -1.67, 0.21, 0.3, 0};
static std::vector<float> arm1WorkJointOrder{-30 * M_PI/180, -74.68 * M_PI/180, -84.98 * M_PI/180, 18.97 * M_PI/180, 34.89 * M_PI/180, -38.42 * M_PI/180};

static std::vector<float> arm2RestJointOrder {10.5 * M_PI/180, -151.3 * M_PI/180, 142.1 * M_PI/180, 8.9 * M_PI/180, 11.08 * M_PI/180, -1.21 * M_PI/180};
static std::vector<float> arm2IntermediaryJointOrder {0.048, -2.6, 2.2, -2.2, 0.06, 0};
static std::vector<float> arm2WorkJointOrder {25.6 * M_PI/180, -102.26 * M_PI/180, 82.79 * M_PI/180, -207.4 * M_PI/180, -32.8 * M_PI/180, 41.54 * M_PI/180};




static void notifySupervisorTaskDone(void);

static std::string sensorString;

void robotPositionPublish(void);

class pubHandler
{
public:
	ros::Publisher pubSkilledWorkcellStatus;

	ros::Publisher pubRobotPositionArm1;//Publisher commun pour que ceux qui donnent l'ordre de position savent où on est
	ros::Publisher pubRobotPositionArm2;
};

pubHandler pubScoped;


static void notifySupervisorTaskDone(void)
{
	ROS_INFO("Notifying supervisor we're done");

	std_msgs::Bool msgToSend;
	msgToSend.data = true;
	pubScoped.pubSkilledWorkcellStatus.publish(msgToSend);
}

skilledWorkcellInterface& getSkilledWorkcellInterface(void)
{
	static michelinSkilledWorkcellInterface retval;
	return retval;
}

void michelinSkilledWorkcellInterface::skilledWorkcellInit(void)
{
	if(lastOrder != INIT)
	{
		std::cout << "Call polymorphed version of the SkilledWorkcell interface : Init" << std::endl;

		lastOrder = INIT;
		currentMaster = SKILLED_WORKCELL;
		PLC_Request_Init();

		gripper_init(1);
		gripper_init(2);
		gripper_init(4);
		gripper_init(5);

		gripper_open(1);
		gripper_open(2);
		gripper_open(4);
		gripper_open(5);

		//Integrer ici des mouvements sur le first time
		stopCurrentController(ARM1);
		stopCurrentController(ARM2);

		startCurrentController(ARM1, JOINT_CONTROLLER);
		startCurrentController(ARM2, JOINT_CONTROLLER);

		jointMoveArm(ARM1, arm1IntermediaryJointOrder);
		jointMoveArm(ARM2, arm2IntermediaryJointOrder);

		ros::Rate loop_rate(50);
		int count = 100;
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		jointMoveArm(ARM1, arm1WorkJointOrder);
		jointMoveArm(ARM2, arm2WorkJointOrder);

		count = 100; //2 secondes
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}
		notifySupervisorTaskDone();
	}
}

void michelinSkilledWorkcellInterface::skilledWorkcellIdle(void)
{
	if(lastOrder != IDLE)
	{
		std::cout << "Call polymorphed version of the skilledWorkcell interface : Idle" << std::endl;
		lastOrder = IDLE;
		currentMaster = SKILLED_WORKCELL;

		stopCurrentController(ARM1); //Prevent Red lines in polyscope
		stopCurrentController(ARM2);
	}
}

void michelinSkilledWorkcellInterface::skilledWorkcellStop(void)
{
	if(lastOrder != STOP)
	{
		std::cout << "Call polymorphed version of the skilledWorkcell interface : Stop" << std::endl;

		gripper_open(1);
		gripper_open(2);
		gripper_open(4);
		gripper_open(5);

		ros::Rate loop_rate(50);
		int count = 50;//Laisser le temps au pince de s'ouvrir
		//TODO : poll les grippers pour savoir s'ils ont réussi?
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		std::cout << "Requesting plc thread to join" << std::endl;
		PLC_Request_End();
		std::cout << "plc thread joined" << std::endl;
		lastOrder = STOP;
		currentMaster = SKILLED_WORKCELL;

		//Revenir a une position "defaut"

		//Integrer ici des mouvements sur le first time
		stopCurrentController(ARM1);
		stopCurrentController(ARM2);

		startCurrentController(ARM1, JOINT_CONTROLLER);
		startCurrentController(ARM2, JOINT_CONTROLLER);

		jointMoveArm(ARM1, arm1IntermediaryJointOrder);
		jointMoveArm(ARM2, arm2IntermediaryJointOrder);

		count = 200;//Sinon le bras droit est beaucoup trop con
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		/*jointMoveArm(ARM1, arm1RestJointOrder);
		jointMoveArm(ARM2, arm2RestJointOrder); //Celui la tape et ça me gonfle

		count = 100; //2 secondes
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}*/

		notifySupervisorTaskDone();
	}
}

void michelinSkilledWorkcellInterface::skilledWorkcellPerformT1S2(void)
{
	robotPositionPublish();
	if(lastOrder != PERFORMT1S2)
	{
		std::cout << "Call polymorphed version of the skilledWorkcell interface : PerformT1S2" << std::endl;

		lastOrder = PERFORMT1S2;
		currentMaster = SUPERVISOR;

		stopCurrentController(ARM1);
		stopCurrentController(ARM2);

		startCurrentController(ARM1, POSITION_CONTROLLER);
		startCurrentController(ARM2, POSITION_CONTROLLER);
	}

	//TODO : creer une socket qui sniff python. Envoyer a skilledWorkcell les ordres en fonctions des boutons appuyes.
}

void michelinSkilledWorkcellInterface::skilledWorkcellGraspingT1S3(void)
{
	robotPositionPublish();

	if(lastOrder != GRASPINGT1S3)
	{
		std::cout << "Call polymorphed version of the skilledWorkcell interface : GraspingT1S3" << std::endl;
		lastOrder = GRASPINGT1S3;
		currentMaster = SKILLED_WORKCELL;

		stopCurrentController(ARM1);
		stopCurrentController(ARM2);

		startCurrentController(ARM1, POSITION_CONTROLLER);
		startCurrentController(ARM2, POSITION_CONTROLLER);

		int count = 5;
		ros::Rate loop_rate(50);
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		pose arm1TargetPoseAbsolute = getArm1Pose();
		pose arm2TargetPoseAbsolute = getArm2Pose();

		std::cout << "position gauche :" << arm1TargetPoseAbsolute.x << "  " << arm1TargetPoseAbsolute.y << "  " << arm1TargetPoseAbsolute.z << std::endl;
		std::cout << "position droite :" << arm2TargetPoseAbsolute.x << "  " << arm2TargetPoseAbsolute.y << "  " << arm2TargetPoseAbsolute.z << std::endl;

		std::cout << "orientation gauche :" << arm1TargetPoseAbsolute.roll << "  " << arm1TargetPoseAbsolute.pitch << "  " << arm1TargetPoseAbsolute.yaw << std::endl;
		std::cout << "orientation droite :" << arm2TargetPoseAbsolute.roll << "  " << arm2TargetPoseAbsolute.pitch << "  " << arm2TargetPoseAbsolute.yaw << std::endl;

		/*arm1TargetPoseAbsolute.x = 0.114;
		arm1TargetPoseAbsolute.y = -0.35;
		arm1TargetPoseAbsolute.z = 0.06;
		arm1TargetPoseAbsolute.roll = 1.275;
		arm1TargetPoseAbsolute.pitch = -0.20;
		arm1TargetPoseAbsolute.yaw = -0.1;

		arm2TargetPoseAbsolute.x = 0.12;
		arm2TargetPoseAbsolute.y = 0.35;
		arm2TargetPoseAbsolute.z = 0.03;
		arm2TargetPoseAbsolute.roll = 1.46;
		arm2TargetPoseAbsolute.pitch = -2.97;
		arm2TargetPoseAbsolute.yaw = -0.08;*/

		arm1TargetPoseAbsolute.y = -0.31;
		arm2TargetPoseAbsolute.y = 0.30;

		arm1TargetPoseAbsolute.z += 0.002;
		arm2TargetPoseAbsolute.z += 0.002;

		arm2TargetPoseAbsolute.x -= 0.015;


		cartesianPositionMoveArm(ARM1, arm1TargetPoseAbsolute);
		cartesianPositionMoveArm(ARM2, arm2TargetPoseAbsolute);

		//Wait and run callbacks
		count = 300;
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		arm1TargetPoseAbsolute.y += 0.03;
		arm1TargetPoseAbsolute.z -= 0.05;
		arm1TargetPoseAbsolute.roll = 1.66674;
		arm1TargetPoseAbsolute.pitch = 3.08579;
		arm1TargetPoseAbsolute.yaw = 0;

		//arm2TargetPoseAbsolute.y += 0.03;
   	arm2TargetPoseAbsolute.z -= 0.05;
		arm2TargetPoseAbsolute.roll = 1.68704;
		arm2TargetPoseAbsolute.pitch = 0;
		arm2TargetPoseAbsolute.yaw = 0;

		cartesianPositionMoveArm(ARM1, arm1TargetPoseAbsolute);
		cartesianPositionMoveArm(ARM2, arm2TargetPoseAbsolute);

		count = 300;
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		gripper_close(1);
		gripper_close(2);
		gripper_close(4);
		gripper_close(5);

		count = 50;
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		gripper_close(1);
		gripper_close(2);
		gripper_close(4);
		gripper_close(5);

		arm1TargetPoseAbsolute.x += 0.03;
		arm1TargetPoseAbsolute.z += 0.05;
		arm1TargetPoseAbsolute.pitch = -3.08579*3/4;

		arm2TargetPoseAbsolute.x += 0.03;
		arm2TargetPoseAbsolute.z += 0.05;
		arm2TargetPoseAbsolute.pitch = -3.08579*1/4;

		cartesianPositionMoveArm(ARM1, arm1TargetPoseAbsolute);

		count = 200;
		cartesianPositionMoveArm(ARM2, arm2TargetPoseAbsolute);
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		arm1TargetPoseAbsolute.x += 0.03;
		//arm1TargetPoseAbsolute.y += 0.03;//Pour le cintrage, a allier avec une rotation?
		arm1TargetPoseAbsolute.z += 0.05;
		//arm1TargetPoseAbsolute.roll = 1.66674*10/8; //C'est sur ce parametre qu'on peut jouer avec le cintrage. TODO : trouver le sens exact
		//arm1TargetPoseAbsolute.yaw = 1.66674*1/4; //C'est sur ce parametre qu'on peut jouer avec le cintrage. TODO : trouver le sens exact

		arm2TargetPoseAbsolute.x += 0.03;
		//arm2TargetPoseAbsolute.y += -0.03;//Pour le cintrage, a allier avec une rotation?
		arm2TargetPoseAbsolute.z += 0.05;
		//arm2TargetPoseAbsolute.yaw = 1.66674*1/4; //C'est sur ce parametre qu'on peut jouer avec le cintrage. TODO : trouver le sens exact
		//arm2TargetPoseAbsolute.roll = 1.66674*10/8;


		//TODO : fix orientation. This is RX, RY, RZ (radian)
		/*arm1TargetPoseAbsolute.roll = 0.20;
		arm1TargetPoseAbsolute.pitch = -3.26;
		arm1TargetPoseAbsolute.yaw = -1.37;*/

		//TODO : fix orientation. This is RX, RY, RZ (radian)
		/*arm2TargetPoseAbsolute.roll = 0.48;
		arm2TargetPoseAbsolute.pitch = 3.3;
		arm2TargetPoseAbsolute.yaw = 1.66;*/

		cartesianPositionMoveArm(ARM1, arm1TargetPoseAbsolute);
		cartesianPositionMoveArm(ARM2, arm2TargetPoseAbsolute);

		count = 150;
		for(int i = 0; i < count; i++)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}

		gripper_close(1);
		gripper_close(2);
		gripper_close(4);
		gripper_close(5);

	}
}

void michelinSkilledWorkcellInterface::skilledWorkcellPerformT1S3(void)
{
	robotPositionPublish();

	if(lastOrder != PERFORMT1S3)
	{
		std::cout << "Call polymorphed version of the skilledWorkcell interface : PerformT1S3" << std::endl;

		lastOrder = PERFORMT1S3;
		currentMaster = SKILLED_WORKCELL;

		stopCurrentController(ARM1);
		stopCurrentController(ARM2);

		startCurrentController(ARM1, SPEED_CONTROLLER);
		startCurrentController(ARM2, SPEED_CONTROLLER);
	}
}

nodeID specificSkilledWorkcellGetMaster(void)
{
	return currentMaster;
}

void specificSkilledWorkcellControl(std::string commandString, std::string sensorString)
{
	ROS_INFO("start controlling robot");

	std::stringstream commandStringStream(commandString);
	std::stringstream sensorStringStream(sensorString);

	boost::archive::text_iarchive ia(commandStringStream);
	pose moveOrderLeft, moveOrderRight;
	ia >> moveOrderLeft;
	ia >> moveOrderRight;



	cartesianPositionMoveArm(ARM1, moveOrderLeft);
	cartesianPositionMoveArm(ARM2, moveOrderRight);

	ROS_INFO("stop controlling robot");

	return;
}


void specificSkilledWorkcellRosInit()
{
	UR10RosInit();
	ros::NodeHandle nh;
	pubScoped.pubSkilledWorkcellStatus = nh.advertise<std_msgs::Bool>("skilledWorkcellStepDone", 1);

	pubScoped.pubRobotPositionArm1 = nh.advertise<campero_ur_ip_controllers::PoseRPY>("RobotPositionArm1", 1);
	pubScoped.pubRobotPositionArm2 = nh.advertise<campero_ur_ip_controllers::PoseRPY>("RobotPositionArm2", 1);
}

void specificSkilledWorkcell_setSensor(std::string param)
{
	sensorString = param;
}

void robotPositionPublish(void)
{
	campero_ur_ip_controllers::PoseRPY msgToSend;

	pose robotPose = getArm1Pose();
	msgToSend.position.x = robotPose.x;
	msgToSend.position.y = robotPose.y;
	msgToSend.position.z = robotPose.z;
	msgToSend.orientation.roll = robotPose.roll;
	msgToSend.orientation.pitch = robotPose.pitch;
	msgToSend.orientation.yaw = robotPose.yaw;
	pubScoped.pubRobotPositionArm1.publish(msgToSend);

	robotPose = getArm2Pose();
	msgToSend.position.x = robotPose.x;
	msgToSend.position.y = robotPose.y;
	msgToSend.position.z = robotPose.z;
	msgToSend.orientation.roll = robotPose.roll;
	msgToSend.orientation.pitch = robotPose.pitch;
	msgToSend.orientation.yaw = robotPose.yaw;
	pubScoped.pubRobotPositionArm2.publish(msgToSend);
}
