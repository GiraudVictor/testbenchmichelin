#include "gripper.hpp"

#include <chrono>
#include <iostream>
#include <thread>

#include "PLC.hpp"


const std::string gripperTagNamesOut[] = {"GRIPPER_G_1_out", "GRIPPER_G_2_out", "GRIPPER_G_3_out", "GRIPPER_D_1_out", "GRIPPER_D_2_out", "GRIPPER_D_3_out"};
const std::string gripperTagNamesIn[] = {"GRIPPER_G_1_in", "GRIPPER_G_2_in", "GRIPPER_G_3_in", "GRIPPER_D_1_in", "GRIPPER_D_2_in", "GRIPPER_D_3_in"};

//Small gripper order list
const std::string stepBusy = ".b_StepBusy";
const std::string stepDone = ".b_StepDone";
const std::string error = ".b_error";
const std::string motionError = ".b_MotionError";
const std::string dataTransferRequired = ".b_DataTransferRequired";
const std::string dataTransferOk = ".b_DataTransferOK";
const std::string MotorOn = ".b_MotorON";

//Big gripper order list
const std::string ControlWord100 = ".b_ControlWord_100"; //Bit contraining moveToBase flag
const std::string ControlWord200 = ".b_ControlWord_200"; //Bit contraining moveToWork flag

const std::string commandAdjust = ".cmd_b_Adjust";
const std::string commandDataTransfer = ".cmd_b_DataTransfer";
const std::string commandMoveToBase = ".cmd_b_MoveToBase";
const std::string commandMoveToWork = ".cmd_b_MoveToWork";
const std::string commandResetDirectionFlag = ".cmd_b_ResetDirectionFlag";
const std::string commandStepReset = ".cmd_b_StepReset";
const std::string commandWritePDU = ".cmd_b_WritePDU";
const std::string commandJogToBase = ".cmd_b_JogToBase";
const std::string commandJogToWork = ".cmd_b_JogToWork";
const std::string commandErrorReset = ".cmd_b_ErrorReset";

const std::string deviceMode = ".n_DeviceMode";
const std::string GripForce = ".n_GripForce";
const std::string PositionTolerance = ".n_PositionTolerance";

const std::string TeachPosition = ".n_TeachPosition";
const std::string BasePosition = ".n_BasePosition";
const std::string ShiftPosition = ".n_ShiftPosition";
const std::string WorkPosition = ".n_WorkPosition";
const std::string Workpiece = ".n_WorkpieceNo";
const std::string DriveVelocity = ".n_DriveVelocity";
const std::string Diagnose = ".n_Diagnose";
const std::string MotionTimeout = ".t_MotionTimeout_pre";


//I'm only dealing with small gripper in that case : number 1,2, 4 and 5.

void asyncOpen(int gripperNumber);
void asyncClose(int gripperNumber);

void handshake(int gripperNumber);
void controlMovement(int gripperNumber);
void clean_recoverable_diag(int gripperNumber);


void gripper_init(int gripperNumber)//Synchronous cause we don't want to send out stuff to an uninitialized gripper
{
  std::string basicNameStringRead = gripperTagNamesOut[gripperNumber];
	std::string basicNameStringWrite = gripperTagNamesIn[gripperNumber];

	//reset cmd word
  PLC_Request_WriteTag(basicNameStringWrite + commandMoveToWork, 0);
	PLC_Request_WriteTag(basicNameStringWrite + commandMoveToBase, 0);
	PLC_Request_WriteTag(basicNameStringWrite + commandStepReset, 0);

	//device_mode = 100;
	PLC_Request_WriteTag(basicNameStringWrite + deviceMode, 100);

	//GripForce = 1;
	PLC_Request_WriteTag(basicNameStringWrite + GripForce, 4);

	//PositionTolerance = 255
	PLC_Request_WriteTag(basicNameStringWrite + PositionTolerance, 1);

	//TeachPosition = 1600
	PLC_Request_WriteTag(basicNameStringWrite + TeachPosition, 0);
	//Timo : don't need, only feedback. Can put to 0

	//Workpiece = 0
	PLC_Request_WriteTag(basicNameStringWrite + Workpiece, 0);

	//MotionTimeout = 2000
	PLC_Request_WriteTag(basicNameStringWrite + MotionTimeout, 2000);

	//check_step_busy
	PLC_Request_WriteTag(basicNameStringWrite + commandStepReset, 1);

  while(PLC_Request_ReadTag(basicNameStringRead + stepBusy) == 1)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
	PLC_Request_WriteTag(basicNameStringWrite + commandStepReset, 0);

	handshake(gripperNumber);
}

void gripper_open(int gripperNumber)
{
  //std::thread gripperThread(asyncOpen, gripperNumber);//launch the thread
  //gripperThread.detach();
  asyncOpen(gripperNumber);
}

void gripper_close(int gripperNumber)
{
  //std::thread gripperThread(asyncClose, gripperNumber);//launch the thread
  //gripperThread.detach();
  asyncClose(gripperNumber);
}


void asyncOpen(int gripperNumber)
{
  PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToBase, 1);

  while(PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + ControlWord100) == 0)
  {
    controlMovement(gripperNumber);
    clean_recoverable_diag(gripperNumber);

    PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToBase, 0); //Reset of rising/falling edge. Necessary for the PLC block, not the gripper
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToBase, 1);
  }
  PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToBase, 0);
}

void asyncClose(int gripperNumber)
{
  PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToWork, 1);

  while(PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + ControlWord200) == 0)
  {
    controlMovement(gripperNumber);
    clean_recoverable_diag(gripperNumber);

    PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToWork, 0); //Reset of rising/falling edge. Necessary for the PLC block, not the gripper
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToWork, 1);
  }

  PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandMoveToWork, 0);
}

void controlMovement(int gripperNumber)
{
  //check step busy
  if(PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + stepBusy) == 1)
  {
    PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandStepReset, 1);
    while(PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + stepBusy) == 1)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandStepReset, 0);
  }
  //check diag
  int diag = PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + Diagnose);
  if(diag != 0)
  {
    if(diag == 775)
    {
      PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandResetDirectionFlag, 1);
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      PLC_Request_WriteTag(gripperTagNamesIn[gripperNumber] + commandResetDirectionFlag, 0);
    }
    else
    {
      std::cout << "unhandled diag on gripper " << gripperNumber << " : error Number " << diag << std::endl;
      //TODO : terminate thread here?
    }
  }
}

void clean_recoverable_diag(int gripperNumber)
{
  std::string basicNameStringRead = gripperTagNamesOut[gripperNumber];
  std::string basicNameStringWrite = gripperTagNamesIn[gripperNumber];

  //reset cmd word
  PLC_Request_WriteTag(basicNameStringWrite + commandAdjust, 0);
  PLC_Request_WriteTag(basicNameStringWrite + commandDataTransfer, 0);
  PLC_Request_WriteTag(basicNameStringWrite + commandMoveToBase, 0);
  PLC_Request_WriteTag(basicNameStringWrite + commandMoveToWork, 0);
  PLC_Request_WriteTag(basicNameStringWrite + commandResetDirectionFlag, 0);
  PLC_Request_WriteTag(basicNameStringWrite + commandStepReset, 0);
  PLC_Request_WriteTag(basicNameStringWrite + commandWritePDU, 0);

  //check diag
  int diagNumber = PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + Diagnose);
  std::cout << "Diag number" << diagNumber << std::endl;

  while(PLC_Request_ReadTag(gripperTagNamesOut[gripperNumber] + Diagnose) == 1024)
  {
      PLC_Request_WriteTag(basicNameStringWrite + commandStepReset, 1);

      while(PLC_Request_ReadTag(basicNameStringRead + stepBusy) == 1)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
      }

      PLC_Request_WriteTag(basicNameStringWrite + commandErrorReset, 1);
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
      PLC_Request_WriteTag(basicNameStringWrite + commandErrorReset, 0);
  }
}

void handshake(int gripperNumber)
{
  	std::string basicNameStringRead = gripperTagNamesOut[gripperNumber];
  	std::string basicNameStringWrite = gripperTagNamesIn[gripperNumber];

  	//reset cmd word
  	PLC_Request_WriteTag(basicNameStringWrite + commandAdjust, 0);
  	PLC_Request_WriteTag(basicNameStringWrite + commandDataTransfer, 0);
  	PLC_Request_WriteTag(basicNameStringWrite + commandMoveToBase, 0);
  	PLC_Request_WriteTag(basicNameStringWrite + commandMoveToWork, 0);
  	PLC_Request_WriteTag(basicNameStringWrite + commandResetDirectionFlag, 0);
  	PLC_Request_WriteTag(basicNameStringWrite + commandStepReset, 0);
  	PLC_Request_WriteTag(basicNameStringWrite + commandWritePDU, 0);

  	//Control word 01 : data transfer
  	PLC_Request_WriteTag(basicNameStringWrite + commandDataTransfer, 1);

  	//Status Bit 12 = true
  	while(PLC_Request_ReadTag(basicNameStringRead + dataTransferOk) == 0)
		{
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    //Control word 00 : end of data transfer
  	PLC_Request_WriteTag(basicNameStringWrite + commandDataTransfer, 0);

  	//Status Bit 12 = false
    while(PLC_Request_ReadTag(basicNameStringRead + dataTransferOk) == 0)
		{
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
