//This object integrate the use of a mesh for deformable object
//The mesh includes a vector of nodes, and a vector of triangles representing links between nodes.

#include "json.hpp"

struct node
{
	node(float X, float Y, float Z):x(X), y(Y), z(Z){};
	float x;
	float y;	
	float z;	
};

//TODO : make points private, and getters
class triangle
{
public:
	triangle();
	triangle(int a, int b, int c):firstPoint(a), secondPoint(b), thirdPoint(c){};
	int firstPoint;
	int secondPoint;
	int thirdPoint;
private:
};

//TODO : constructeur depuit json
class mesh
{
public:
	mesh();
	mesh(nlohmann::json);
	void update(nlohmann::json);
	nlohmann::json toJson(void);	
	std::vector<node> getNodes(void){return nodeVector;};
	std::vector<triangle> getTriangles(void){return triangleVector;};
private:	
	std::vector<node> nodeVector;
	std::vector<triangle> triangleVector;
};


