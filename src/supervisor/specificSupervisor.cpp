#include "specificSupervisor.hpp"
#include <ros/ros.h>
#include <time.h>// For sleep
#include <iostream>
#include "../common/zhelpers.hpp"
#include "../common/PoseRPY.h"

#include <std_msgs/String.h>
#include <std_msgs/Bool.h>

static softmanbotState previousReadState = IDLE;

static bool isStepDone = false;

static zmq::context_t context(1);
static zmq::socket_t pairSocket (context, ZMQ_PAIR); //TODO set sockOp NOblock
static zmq::socket_t orderSocket (context, ZMQ_PAIR); //TODO set sockOp NOblock

static void callbackPosArm1(const campero_ur_ip_controllers::PoseRPY::ConstPtr& msg);
static void callbackPosArm2(const campero_ur_ip_controllers::PoseRPY::ConstPtr& msg);

static pose poseArm1;
static pose poseArm2;

class pubHandler
{
public:
	ros::Publisher workcellControl;
	ros::Subscriber poseArm1Sub;
	ros::Subscriber poseArm2Sub;
};

pubHandler pubScoped;

supervisoryInterface& getSupervisoryInterface(void)
{
	static michelinSupervisoryInterface retval;
	return retval;
}

softmanbotState state_machine(void)
{

	static bool isFirstTime = true;

	if(isFirstTime)
	{
		isFirstTime = false;
		pairSocket.bind("tcp://127.0.0.1:5556");//Localhost
		orderSocket.bind("tcp://127.0.0.1:5656");
	}


	//std::cout << "Waiting for receive" << std::endl;

  std::string contents = s_recv (pairSocket);
	//std::cout << "received " << contents << std::endl;

	if(isStepDone)
	{
		s_send(pairSocket, "StepDone");
	}
	else
	{
		s_send(pairSocket, "StepNotDone");
	}

	int IHMState = std::stoi(contents);

	switch(IHMState)
	{
	case 0:
		if(IDLE != previousReadState)
		{
			isStepDone = false;
			previousReadState = IDLE;
		}
		return IDLE;
		break;
	case 1:
		if(INIT != previousReadState)
		{
			isStepDone = false;
			previousReadState = INIT;
		}
		return INIT;
		break;
	case 2:
		if(STOP != previousReadState)
		{
			isStepDone = false;
			previousReadState = STOP;
		}
		return STOP;
		break;
	case 6:
			if(PERFORMT1S2 != previousReadState)
			{
				isStepDone = false;
				previousReadState = PERFORMT1S2;
			}
			return PERFORMT1S2;
			break;
	case 7:
		if(GRASPINGT1S3 != previousReadState)
		{
			isStepDone = false;
			previousReadState = GRASPINGT1S3;
		}
		return GRASPINGT1S3;
		break;
	case 8:
		if(PERFORMT1S3 != previousReadState)
		{
			isStepDone = false;
			previousReadState = PERFORMT1S3;
		}
		return PERFORMT1S3;
		break;

	default:
		throw(std::string("exception : Invalid state required"));
		break;
	}
}

void specificSupervisoryRosInit(void)
{
	//Not that good, need absolute path because of roslaunch
	system("python3 /home/victor/Bureau/TestbenchMichelin/src/supervisor/IHM.py &"); //& necessary else it blocks current thread until HIM die.

	ros::NodeHandle nh;
	pubScoped.workcellControl = nh.advertise<std_msgs::String>("supervisoryControl", 1);
	pubScoped.poseArm1Sub = nh.subscribe("RobotPositionArm1", 1, callbackPosArm1);
	pubScoped.poseArm2Sub = nh.subscribe("RobotPositionArm2", 1, callbackPosArm2);
}

void michelinSupervisoryInterface::supervisoryInit(void)
{
	//std::cout << "Call polymorphed version of the interface : Init" << std::endl;
	isStepDone = true;
}

void michelinSupervisoryInterface::supervisoryIdle(void)
{
	//std::cout << "Call polymorphed version of the interface : Idle" << std::endl;
	isStepDone = true;
}

void michelinSupervisoryInterface::supervisoryStop(void)
{
	//std::cout << "Call polymorphed version of the interface : Stop" << std::endl;
	isStepDone = true;
}

void michelinSupervisoryInterface::supervisoryPerformT1S2(void)
{
	//std::cout << "Call polymorphed version of the interface : PerformT1S2" << std::endl;
	static bool isOrderInProgress = false;

	zmq::message_t message;

	std::string contents = s_recv (orderSocket);
	std::cout << contents << std::endl;

	if((contents != "Nothing") && (!isOrderInProgress))
	{
		//Depouiller le texte reçu
		pose targetPose;
		bool isTargetLeft = true;
		float poseMod;
		if(contents[0] == 'l')
		{
			targetPose = poseArm1;
		}
		else if(contents[0] == 'r')
		{
			isTargetLeft = false;
			targetPose = poseArm2;
		}
		else
		{
			throw("unexpected message received :", contents);
		}
		if(contents[2] == '+')
		{
			poseMod = 0.05;
		}
		else if(contents[2] == '-')
		{
			poseMod = -0.05;
		}
		else
		{
			throw("unexpected message received :", contents);
		}
		if(contents[1] == 'x')
		{
			targetPose.x += poseMod;
		}
		else if(contents[1] == 'y')
		{
			targetPose.y += poseMod;
		}
		else if(contents[1] == 'z')
		{
			targetPose.z += poseMod;
		}
		else
		{
			throw("unexpected message received :", contents);
		}
		isOrderInProgress = true;
		//TODO : send order

		//Create absolute pose
		//send them to external_control

		std::stringstream ssSend;
		boost::archive::text_oarchive oa(ssSend);

		if(isTargetLeft)
		{

			std::cout << "moving arm from" << poseArm1.x << "  " << poseArm1.y << "  " << poseArm1.z << std::endl;
			std::cout << "to " <<  targetPose.x << "  " <<  targetPose.y << "  " <<  targetPose.z << std::endl;

			oa << targetPose;
			oa << poseArm2;
		}
		else
		{
			std::cout << "moving arm from" << poseArm2.x << "  " << poseArm2.y << "  " << poseArm2.z << std::endl;
			std::cout << "to " <<  targetPose.x << "  " <<  targetPose.y << "  " <<  targetPose.z << std::endl;

			oa << poseArm1;
			oa << targetPose;
		}
		std_msgs::String msg;
		msg.data = ssSend.str();
		pubScoped.workcellControl.publish(msg);
	}

	//TODO : check if orderFinished. Check si position courante et position cible sont à - de 1mm ?
	isOrderInProgress = false;

	//When contents are received, stepDone go to false.
}

void michelinSupervisoryInterface::supervisoryGraspingT1S3(void)
{
	std::cout << "Call polymorphed version of the interface : GraspingT1S3" << std::endl;
	isStepDone = true;

}
void michelinSupervisoryInterface::supervisoryPerformT1S3(void)
{
	std::cout << "Call polymorphed version of the interface : PerformT1S3" << std::endl;
	isStepDone = true;
}


static void callbackPosArm1(const campero_ur_ip_controllers::PoseRPY::ConstPtr& msg)
{
	pose tempPose(msg->position.x, msg->position.y, msg->position.z, msg->orientation.roll, msg->orientation.pitch, msg->orientation.yaw);
	poseArm1 = tempPose;

}

static void callbackPosArm2(const campero_ur_ip_controllers::PoseRPY::ConstPtr& msg)
{
	std::cout << "Callback position arm 2" << std::endl;

	pose tempPose(msg->position.x, msg->position.y, msg->position.z, msg->orientation.roll, msg->orientation.pitch, msg->orientation.yaw);
	poseArm2 = tempPose;
}
