from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer
import sys
import time
import zmq
import matplotlib
import os
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)

        self.axes = fig.add_subplot(111, projection='3d')
        super(MplCanvas, self).__init__(fig)


class Socket():
    def __init__(self):
        context = zmq.Context()
        self._pairsocket = context.socket(zmq.PAIR)
        self._pairsocket.connect("tcp://127.0.0.1:5556")

        self._ordersocket = context.socket(zmq.PAIR)
        self._ordersocket.connect("tcp://127.0.0.1:5656")

		#self._pubsocket = context.socket(zmq.PUB)
        #self._pubsocket.bind("tcp://*:5558")


    def pairReceive(self):
        msg = self._pairsocket.recv()
        return msg

    def pairSend(self, msgToSend):
        self._pairsocket.send_string(msgToSend)

    def orderSend(self, msgToSend):
        self._ordersocket.send_string(msgToSend)

class GroupBox(QWidget):

    def __init__(self):
        QWidget.__init__(self)

        self._socket = Socket()

        self.setWindowTitle("GroupBox")
        layout = QGridLayout()
        self.setLayout(layout)

        groupbox = QGroupBox("State Sender")
        layout.addWidget(groupbox, 0, 0)

        #Radio button for dealing with states
        self.vboxRadio = QVBoxLayout()
        groupbox.setLayout(self.vboxRadio)

        radiobutton = QRadioButton("Idle")
        radiobutton.setChecked(True)
        radiobutton.state = "idle"
        radiobutton.toggled.connect(self.radioOnClicked)
        self.vboxRadio.addWidget(radiobutton)

        self._state = "idle"

        radiobutton = QRadioButton("Init")
        radiobutton.state = "init"
        radiobutton.toggled.connect(self.radioOnClicked)
        self.vboxRadio.addWidget(radiobutton)

        radiobutton = QRadioButton("Stop")
        radiobutton.state = "stop"
        radiobutton.toggled.connect(self.radioOnClicked)
        self.vboxRadio.addWidget(radiobutton)

        radiobutton = QRadioButton("Grasp")
        radiobutton.state = "grasp"
        radiobutton.toggled.connect(self.radioOnClicked)
        self.vboxRadio.addWidget(radiobutton)

        radiobutton = QRadioButton("Perform")
        radiobutton.state = "perform"
        radiobutton.toggled.connect(self.radioOnClicked)
        self.vboxRadio.addWidget(radiobutton)

        radiobutton = QRadioButton("IHMControl") #On va mapper ca sur T1S2
        radiobutton.state = "ihmcontrol"
        radiobutton.toggled.connect(self.radioOnClicked)
        self.vboxRadio.addWidget(radiobutton)

        #group of buttons for the robots
        groupbox = QGroupBox("LeftRobot")
        layout.addWidget(groupbox, 1, 0, 1, 2)

        self.leftRobotButtons = QGridLayout()
        groupbox.setLayout(self.leftRobotButtons)

        button = QPushButton("x-")
        button.clicked.connect(self.leftRobotButtonOnClicked)
        self.leftRobotButtons.addWidget(button, 0, 0)
        button = QPushButton("x+")
        button.clicked.connect(self.leftRobotButtonOnClicked)
        self.leftRobotButtons.addWidget(button, 0, 1)

        button = QPushButton("y-")
        button.clicked.connect(self.leftRobotButtonOnClicked)
        self.leftRobotButtons.addWidget(button, 1, 0)
        button = QPushButton("y+")
        button.clicked.connect(self.leftRobotButtonOnClicked)
        self.leftRobotButtons.addWidget(button, 1, 1)

        button = QPushButton("z-")
        button.clicked.connect(self.leftRobotButtonOnClicked)
        self.leftRobotButtons.addWidget(button, 2, 0)
        button = QPushButton("z+")
        button.clicked.connect(self.leftRobotButtonOnClicked)
        self.leftRobotButtons.addWidget(button, 2, 1)

        groupbox = QGroupBox("RightRobot")
        layout.addWidget(groupbox, 1, 2, 1, 2)

        self.rightRobotButtons = QGridLayout()
        groupbox.setLayout(self.rightRobotButtons)

        button = QPushButton("x-")
        button.clicked.connect(self.rightRobotButtonOnClicked)
        self.rightRobotButtons.addWidget(button, 0, 0)
        button = QPushButton("x+")
        button.clicked.connect(self.rightRobotButtonOnClicked)
        self.rightRobotButtons.addWidget(button, 0, 1)

        button = QPushButton("y-")
        button.clicked.connect(self.rightRobotButtonOnClicked)
        self.rightRobotButtons.addWidget(button, 1, 0)
        button = QPushButton("y+")
        button.clicked.connect(self.rightRobotButtonOnClicked)
        self.rightRobotButtons.addWidget(button, 1, 1)

        button = QPushButton("z-")
        button.clicked.connect(self.rightRobotButtonOnClicked)
        self.rightRobotButtons.addWidget(button, 2, 0)
        button = QPushButton("z+")
        button.clicked.connect(self.rightRobotButtonOnClicked)
        self.rightRobotButtons.addWidget(button, 2, 1)

        self.sc = MplCanvas(self, width=5, height=4, dpi=100)
        #self.sc.axes.plot([0,1,2,3,4], [10,1,20,3,40])

        zline = np.linspace(0, 15, 1000)
        xline = np.sin(zline)
        yline = np.cos(zline)
        self.sc.axes.plot3D(xline, yline, zline, 'gray')

        # Data for three-dimensional scattered points
        zdata = 15 * np.random.random(100)
        xdata = np.sin(zdata) + 0.1 * np.random.randn(100)
        ydata = np.cos(zdata) + 0.1 * np.random.randn(100)
        self.sc.axes.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');



        layout.addWidget(self.sc, 0, 1, 1, 3)

        self.leftRobotDisable()
        self.rightRobotDisable()

        #Create timer for recurring callbacks
        self.timer=QTimer()
        self.timer.timeout.connect(self.sendState)
        self.timer.start(500)

        self._msgSent = False

    def radioOnClicked(self):
        radioButton = self.sender()
        if radioButton.isChecked():
            print("State is %s" % (radioButton.state))
            self._state = radioButton.state
            if(self._state == "ihmcontrol"):
                self.leftRobotEnable()
                self.rightRobotEnable()
            else:
                self.leftRobotDisable()
                self.rightRobotDisable()
            #radioButton.setDisabled(True)

        items = (self.vboxRadio.itemAt(i).widget() for i in range(self.vboxRadio.count()))
        for w in items:
            w.setDisabled(True)

    def leftRobotButtonOnClicked(self):
        button = self.sender()
        if(not self._msgSent):
            self._socket.orderSend("l" + button.text())
            self._msgSent = True

    def rightRobotButtonOnClicked(self):
        button = self.sender()
        if(not self._msgSent):
            self._socket.orderSend("r" + button.text())
            self._msgSent = True

    def rightRobotEnable(self):
        items = (self.rightRobotButtons.itemAt(i).widget() for i in range(self.rightRobotButtons.count()))
        for w in items:
            w.setDisabled(False)

    def leftRobotEnable(self):
        items = (self.leftRobotButtons.itemAt(i).widget() for i in range(self.leftRobotButtons.count()))
        for w in items:
            w.setDisabled(False)

    def rightRobotDisable(self):
        items = (self.rightRobotButtons.itemAt(i).widget() for i in range(self.rightRobotButtons.count()))
        for w in items:
            w.setDisabled(True)

    def leftRobotDisable(self):
        items = (self.leftRobotButtons.itemAt(i).widget() for i in range(self.leftRobotButtons.count()))
        for w in items:
            w.setDisabled(True)

    def rightRobotDisable(self):
        items = (self.rightRobotButtons.itemAt(i).widget() for i in range(self.rightRobotButtons.count()))
        for w in items:
            w.setDisabled(True)

    def resetRadio(self):
        items = (self.vboxRadio.itemAt(i).widget() for i in range(self.vboxRadio.count()))
        for w in items:
            w.setDisabled(False)

    def sendState(self):
        #print("trying send")
        #TODO : mapper le state sur l'int qui correspond
        d = {
            'idle': '0',
            'init': '1',
            'stop': '2',
            'ihmcontrol': '6',
            'grasp': '7',
            'perform': '8'
        }
        self._socket.pairSend(d.get(self._state))

        msg = self._socket.pairReceive()
        #print(msg.decode('utf-8'))
        if(msg.decode('utf-8') == "StepDone"):
            self.resetRadio()
        if(self._state == 'ihmcontrol'):
            if(self._msgSent):
                self._msgSent = False
            else:
                self._socket.orderSend("Nothing")

        #TODO : retablir les boutons a la reception du message "skill done"

os.environ["QT_ENABLE_HIGHDPI_SCALING"] = "1"
app = QApplication(sys.argv)
app.setStyle('Fusion')
screen = GroupBox()
screen.show()
sys.exit(app.exec_())
