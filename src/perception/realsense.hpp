#ifndef REALSENSE_HPP
#define REALSENSE_HPP

#include <opencv2/opencv.hpp>

//Prototype of camera hpp
void camera_RosInit(void);
void camera_init(void);
cv::Mat camera_getFrame(void);
void camera_getDepth(void);     //TODO : make it throw in monocular cams

#endif //REALSENSE_HPP
