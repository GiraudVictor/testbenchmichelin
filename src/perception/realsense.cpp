#include "realsense.hpp"

#include <librealsense2/rs.hpp>     // Include RealSense Cross Platform API
#include <librealsense2/rsutil.h>
#include <opencv2/highgui/highgui.hpp>

class RSHandler
{
public:
	rs2::pipeline pipe;
	rs2::config cfg;
	rs2::pipeline_profile selection;

  cv::Mat cameraMatrix;
  cv::Mat distCoeffs;
};

static RSHandler RSCam;

void camera_RosInit(void) //Not a true ros init but will be called only once.
{
  // Video Configuration =====================================
  RSCam.cfg.enable_stream(RS2_STREAM_DEPTH);
  RSCam.cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_BGR8, 60); // u can replace the resolution with 640x480
  RSCam.selection = RSCam.pipe.start(RSCam.cfg);

  //calibration
  auto color_stream = RSCam.selection.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();
  auto intrinsics = color_stream.get_intrinsics();

  float cameraMatrix_temp[9] = { intrinsics.fx, 0., intrinsics.ppx, 0., intrinsics.fy, intrinsics.ppy, 0., 0., 1. };
  RSCam.cameraMatrix = cv::Mat(3, 3, CV_32F, cameraMatrix_temp);
  float distCoeffs_temp[5] = { 0, 0, 0, 0, 0 };
  RSCam.distCoeffs = cv::Mat(1, 5, CV_32F, distCoeffs_temp);
}

void camera_init(void)
{

}

cv::Mat camera_getFrame(void)
{
  // Reading New frame ======================================================
	//Aligning to the 3D camera to RGB ===================================
	rs2::frameset data = RSCam.pipe.wait_for_frames(); // Wait for next set of frames from the camera
	rs2::align align_to_depth(RS2_STREAM_DEPTH);
	rs2::align align_to_color(RS2_STREAM_COLOR);
	data = align_to_color.process(data);

	// Getting information from 3D camera =================================
	rs2::depth_frame depth_orig = data.get_depth_frame();
	rs2::frame depth = data.get_depth_frame();
	rs2::frame color = data.get_color_frame();            // Find the color data

  const int w = depth.as<rs2::video_frame>().get_width();
	const int h = depth.as<rs2::video_frame>().get_height();
	const int w2 = color.as<rs2::video_frame>().get_width();
	const int h2 = color.as<rs2::video_frame>().get_height();

  cv::Mat inputImage(cv::Size(w2, h2), CV_8UC3, (void*)color.get_data(), cv::Mat::AUTO_STEP);

  return inputImage;
}

void camera_getDepth(void)//TODO : make it throw in monocular cams
{

}
