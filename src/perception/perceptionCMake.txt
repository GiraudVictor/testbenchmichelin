find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS roscpp rospy)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS})
find_package(Boost REQUIRED COMPONENTS serialization)

find_package(OpenCV REQUIRED)
find_library(REALSENSE2_FOUND realsense2 HINTS ${LIBRARY_DIR} REQUIRED)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES softmanbot_ros
#  CATKIN_DEPENDS campero_ur_ip_controllers
#  DEPENDS system_lib
)

include_directories( ${catkin_INCLUDE_DIRS})
include_directories(${OpenCV_INCLUDE_DIRS}) # not needed for opencv>=4.0

add_executable(softmanbot_genericPerception_node
	common/genericPerception.cpp common/genericPerception.hpp common/genericLogic.hpp
	perception/specificPerception.cpp perception/specificPerception.hpp perception/realsense.cpp)

target_link_libraries(softmanbot_genericPerception_node
   ${catkin_LIBRARIES}
   ${Boost_LIBRARIES}
   ${OpenCV_LIBS}
   ${REALSENSE2_FOUND}
)

