#include "specificPerception.hpp"

#include "ros/ros.h"
#include "../common/genericLogic.hpp"

#include "realsense.hpp"


static ros::Publisher specificSensorPub;

perceptionInterface& getPerceptionInterface(void)
{
	static michelinPerceptionInterface retval;
	return retval;
}

class pubHandler
{
public:

};

pubHandler pubScoped;


void michelinPerceptionInterface::perceptionInit(void)
{
	cv::Mat inputImage = camera_getFrame();
	cv::imshow("Webcam", inputImage);
	cv::waitKey(1);

}

void michelinPerceptionInterface::perceptionIdle(void)
{

	cv::Mat inputImage = camera_getFrame();
	cv::imshow("Webcam", inputImage);
	cv::waitKey(1);

	/*static int timer = 0;
	timer++;

	if(100 == timer)
	{
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		std::cout << "Saving frame" << std::endl;
		imwrite("/home/victor/Bureau/TestbenchMichelin/test.bmp", inputImage);
	}*/

}

void michelinPerceptionInterface::perceptionStop(void)
{
	cv::Mat inputImage = camera_getFrame();
	cv::imshow("Webcam", inputImage);
	cv::waitKey(1);

}

void michelinPerceptionInterface::perceptionPerformT1S2(void)
{
	cv::Mat inputImage = camera_getFrame();
	cv::imshow("Webcam", inputImage);
	cv::waitKey(1);

}

void michelinPerceptionInterface::perceptionGraspingT1S3(void)
{
	cv::Mat inputImage = camera_getFrame();
	cv::imshow("Webcam", inputImage);
	cv::waitKey(1);
}

void michelinPerceptionInterface::perceptionPerformT1S3(void)
{
	cv::Mat inputImage = camera_getFrame();
	cv::imshow("Webcam", inputImage);
	cv::waitKey(1);
}

void specificPerceptionRosInit(void)
{
	camera_RosInit();


	cv::namedWindow("Webcam", CV_WINDOW_AUTOSIZE);

	return;
}
